# Average Cubic Weight Calculater

This project has been developed on a MAC Mojave x86_64 which comes with python3 already installed. I am using python as the main language to develop the script and bash for project setup.

### In order to run this project in your local environment you need the following dependencies:
 - pip (python version manager)
 - python3.x
 - Bash or sh

### To setup the project just execute the following in your terminal of choice from the root directory:
```bash
sh ./setup.sh
source ./venv/bin/activate
```

### If for any reason the above fails please execute the following commands in your terminal:
```bash
python3 -m venv "venv"
source ./venv/bin/activate
#make sure the ouput is something similar to 
which pip
#~yourdirectory/venv/bin/pip
which python
#~yourdirectory/venv/bin/python

#this should install all of the requirenments for the project
pip install -r requirenments.txt
```
### To run the project please execute in your terminal:
```bash
python average_cubic_weight.py
```

Observe the results and please let me know if you have any questions/concerns about running hte project or the actual code.