import requests
import json

BASE_URI = "http://wp8m3he1wt.s3-website-ap-southeast-2.amazonaws.com"
INITIAL_URI = BASE_URI + "/api/products/1"
CONVERSION_FACTOR = 250

def get_length_in_meters(dimensions={}):
    """
        Return the product of dimensions converted in meters
        - dimensions: Should contain a json object of following structure {width:"",height:"",length:""}
    """
    return (dimensions["width"] * dimensions["height"] * dimensions["length"]) / 1000

def get_average_weight(sum_of_all_weights=0,no_of_entries=0,response={}):
    """
        Recursive function to query all possible api routes and returns the average cubic weight
        - sum_of_all_weights: Default value 0 if not provided. Has to be integer.
        - no_of_weights: Default value 0 if not provided. Has to be integer.
        - response: Default value 0 if not provided. Has to be a response object.
    """
    # iterate over the objects array if possible
    if "objects" in response and response["objects"] is not None:
        # use python list comprehension to extract items which are air conditioners
        items = [item for item in response["objects"] if item["category"] == "Air Conditioners"]
        for item in items:
            weight = get_length_in_meters(item["size"]) * CONVERSION_FACTOR
            sum_of_all_weights += weight
            no_of_entries += 1
        # query the next uri
        if "next" in response and response["next"] is not None:
            next_payload = requests.get(BASE_URI + response["next"]) 
            next_response = json.loads(next_payload.content)
            return get_average_weight(sum_of_all_weights,no_of_entries,next_response)
        # check if the next uri is available to query?
    
    if "next" in response and response["next"] is None:
        print("STEP 2: Contact successfull! Now calculating average cubic height for all Air Conditioners")
        print(f"STEP 3: Number of air conditioners found were {no_of_entries}")
        return sum_of_all_weights/no_of_entries

if __name__ == "__main__":
    # initial payload
    payload = requests.get(INITIAL_URI)
    initial_response = json.loads(payload.content)
    print("STEP 1: Contacting system for product data")
    print(f"STEP 4: Average cubic weight is: {get_average_weight(0,0,initial_response)} kg")