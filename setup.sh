echo "--> Creating virtual environment and installing dependencies"
python3 -m venv "venv"
echo "--> Virtual environment created!"
source ./venv/bin/activate
echo "-->Using pip version"
which pip
echo "-->Using python version"
which python
echo "--> Now installing dependencies"
pip install -r requirenments.txt
echo "-->To complete setup please activate your environment by using command:   source ./venv/bin/activate"
